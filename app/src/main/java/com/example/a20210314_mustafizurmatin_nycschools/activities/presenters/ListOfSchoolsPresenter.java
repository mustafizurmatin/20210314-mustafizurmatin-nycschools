package com.example.a20210314_mustafizurmatin_nycschools.activities.presenters;

import android.content.Context;

import com.example.a20210314_mustafizurmatin_nycschools.activities.contracts.ListOfSchoolContract;

/**
 *
 * @author Mustafizur Matin
 *
 * outlines for Presenter. Given more time I would have implemented MVP architecture.
 * Below is my starting outline.
 */

public class ListOfSchoolsPresenter implements ListOfSchoolContract.Presenter {

    private ListOfSchoolContract.View schoolListView;
    Context context;

    public ListOfSchoolsPresenter(ListOfSchoolContract.View schoolListView) {
        this.schoolListView = schoolListView;
    }

    @Override
    public void requestDataFromServer() {
      /*  Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/resource/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NYCService nycService = retrofit.create(NYCService.class);
        Call<List<SchoolResponse>> call = nycService.getSchoolList();

        call.enqueue(new Callback<List<SchoolResponse>>() {
            @Override
            public void onResponse(Call<List<SchoolResponse>> call, Response<List<SchoolResponse>> response) {
                List<SchoolResponse> schoolResponseList = response.body();
                 schoolListView.setDataToRecyclerView(schoolResponseList);
            }

            @Override
            public void onFailure(Call<List<SchoolResponse>> call, Throwable t) {
                t.printStackTrace();
                //Log.e(TAG, "onFailure: Connection Failure");
            }
        });
*/
    }
}
