package com.example.a20210314_mustafizurmatin_nycschools.activities.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.example.a20210314_mustafizurmatin_nycschools.R;
import com.example.a20210314_mustafizurmatin_nycschools.activities.remote.NYCService;
import com.example.a20210314_mustafizurmatin_nycschools.activities.models.SchoolDetailResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SchoolInformationActivity extends AppCompatActivity {
    private static final String TAG = "TAG";

    TextView nameOfSchool;
    TextView writing_score;
    TextView reading_score;
    TextView math_score;
    TextView num_test_takers;
    String dbn;
    String name;

    private static final String BASE_URL = "https://data.cityofnewyork.us/resource/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_information);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("School Information");

        nameOfSchool = findViewById(R.id.school_name);
        writing_score = findViewById(R.id.writing_score);
        reading_score = findViewById(R.id.reading_score);
        math_score = findViewById(R.id.math_score);
        num_test_takers = findViewById(R.id.num_test_takers);


        Intent intent = getIntent();
        dbn = intent.getStringExtra("dbn");
        name = intent.getStringExtra("name");

        Log.d(TAG, "onCreate: SchoolInformationActivity " );


        getSchoolInformation(dbn,name);

    }

    public void getSchoolInformation(String dbn, final String schoolName){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NYCService nycService = retrofit.create(NYCService.class);
        Call<List<SchoolDetailResponse>> call = nycService.getSchoolDetails(dbn);
        call.enqueue(new Callback<List<SchoolDetailResponse>>() {
            @Override
            public void onResponse(Call<List<SchoolDetailResponse>> call, Response<List<SchoolDetailResponse>> response) {

                if (response.isSuccessful() && response.body().size() > 0){
                    getSchoolInformation(response.body().get(0));
                } else {
                    SchoolDetailResponse schoolDetailResponse = new SchoolDetailResponse();
                    schoolDetailResponse.setSchoolName(schoolName);
                    schoolDetailResponse.setSatWritingAvgScore("unknown");
                    schoolDetailResponse.setSatCriticalReadingAvgScore("unknown");
                    schoolDetailResponse.setSatMathAvgScore("unknown");
                    schoolDetailResponse.setNumOfSatTestTakers("unknown");

                    getSchoolInformation(schoolDetailResponse);
                }

                Log.d(TAG, "onResponse:" + response.body().toString());
            }

            @Override
            public void onFailure(Call<List<SchoolDetailResponse>> call, Throwable t) {
                Log.d(TAG, "onResponse:" + t.getLocalizedMessage());

            }
        });

    }

    /**
     *
     *
     * @param schoolDetailResponse
     *
     * Concatenating strings to provide some more context.
     */

    public void getSchoolInformation(SchoolDetailResponse schoolDetailResponse) {
        String writingText = "Average Writing Score: " + schoolDetailResponse.getSatWritingAvgScore();
        String readingText = "Average Reading Score: " + schoolDetailResponse.getSatCriticalReadingAvgScore();
        String mathText =  "Average Math Score: " + schoolDetailResponse.getSatMathAvgScore();
        String testTakersText = "Number of Test Takers: " + schoolDetailResponse.getNumOfSatTestTakers();

        nameOfSchool.setText(schoolDetailResponse.getSchoolName());
        writing_score.setText(writingText);
        reading_score.setText(readingText);
        math_score.setText(mathText);
        num_test_takers.setText(testTakersText);
    }
}