package com.example.a20210314_mustafizurmatin_nycschools.activities.remote;

import com.example.a20210314_mustafizurmatin_nycschools.activities.models.SchoolDetailResponse;
import com.example.a20210314_mustafizurmatin_nycschools.activities.models.SchoolResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 *
 * @author Mustafizur Matin
 *
 * Networking interface for both schools list and school information.
 *
 */
public interface NYCService {
    /**
     * https://data.cityofnewyork.us/resource/97mf-9njv.json
     *
     * @return
     */
    @GET("s3k6-pzi2.json")
   Call<List<SchoolResponse>> getSchoolList();

    /**
     * https://data.cityofnewyork.us/resource/f9bf-2cp4.json
     *
     * @param dbn
     * @return
     */
    @GET("f9bf-2cp4.json")
    Call<List<SchoolDetailResponse>> getSchoolDetails(@Query("DBN") String dbn);
}