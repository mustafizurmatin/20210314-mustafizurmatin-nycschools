package com.example.a20210314_mustafizurmatin_nycschools.activities.contracts;

import com.example.a20210314_mustafizurmatin_nycschools.activities.models.SchoolResponse;

import java.util.List;

/**
 *
 * @author Mustafizur Matin
 *
 * outlines the relationship between View and Presenter
 *
 * Given more time I would have implemented MVP architecture.
 * Below is my starting outline.
 */


//TODO IMPLEMENT MVP
public interface ListOfSchoolContract {


    interface View {

        /**
         * Provides data to the RecyclerView Adapter
         *
         * @param schoolResponseList:
         */

        void setDataToRecyclerView(List<SchoolResponse> schoolResponseList);

    }


    interface Presenter {

        /**
         * Makes request for data
         */

        void requestDataFromServer();

    }

}
