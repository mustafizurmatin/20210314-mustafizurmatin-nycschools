package com.example.a20210314_mustafizurmatin_nycschools.activities.adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20210314_mustafizurmatin_nycschools.R;
import com.example.a20210314_mustafizurmatin_nycschools.activities.activities.SchoolInformationActivity;
import com.example.a20210314_mustafizurmatin_nycschools.activities.models.SchoolResponse;

import java.util.List;

/**
 *
 * @author Mustafizur Matin
 *
 * RecyclerView.Adapter for list of schools.
 *
 */


public class SchoolListAdapter extends RecyclerView.Adapter<SchoolListAdapter.SchoolListViewHolder> {

    List<SchoolResponse> schoolResponses;

    public SchoolListAdapter(List<SchoolResponse> schoolResponses) {
        this.schoolResponses = schoolResponses;
    }

    @NonNull
    @Override
    public SchoolListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schools_list_items, parent, false);
        return new SchoolListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SchoolListViewHolder holder, final int position) {
        holder.schoolName.setText(schoolResponses.get(position).getSchoolName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //goes to new activity passing the dbn name
                Intent intent = new Intent(holder.itemView.getContext(), SchoolInformationActivity.class);

                //get dbn {key that indentifies each school} for current item
                String dbn = schoolResponses.get(position).getDbn();

                intent.putExtra("dbn", dbn);

                //begin activity
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return schoolResponses.size();
    }

    public class SchoolListViewHolder extends RecyclerView.ViewHolder{
        TextView schoolName;

        public SchoolListViewHolder(@NonNull final View itemView) {
            super(itemView);
            schoolName = itemView.findViewById(R.id.school_name);
        }
    }
}
