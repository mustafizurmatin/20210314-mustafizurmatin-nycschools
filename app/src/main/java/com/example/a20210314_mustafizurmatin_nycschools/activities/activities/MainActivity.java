package com.example.a20210314_mustafizurmatin_nycschools.activities.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.example.a20210314_mustafizurmatin_nycschools.R;
import com.example.a20210314_mustafizurmatin_nycschools.activities.remote.NYCService;
import com.example.a20210314_mustafizurmatin_nycschools.activities.models.SchoolResponse;
import com.example.a20210314_mustafizurmatin_nycschools.activities.adapters.SchoolListAdapter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Mustafizur Matin
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = "Main";
    RecyclerView recyclerView;
    SchoolListAdapter schoolListAdapter;
    List<SchoolResponse> schoolResponseList;

    private static final String BASE_URL = "https://data.cityofnewyork.us/resource/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("NYC HighSchools");

        recyclerView = findViewById(R.id.schools_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, linearLayoutManager.getOrientation());

        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setHasFixedSize(true);

        getSchoolData();
    }

    public void getSchoolData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        NYCService nycService = retrofit.create(NYCService.class);
        Call<List<SchoolResponse>> call = nycService.getSchoolList();

        call.enqueue(new Callback<List<SchoolResponse>>() {
            @Override
            public void onResponse(Call<List<SchoolResponse>> call, Response<List<SchoolResponse>> response) {
                schoolResponseList = response.body();
                schoolListAdapter = new SchoolListAdapter(schoolResponseList);
                recyclerView.setAdapter(schoolListAdapter);

            }
            @Override
            public void onFailure(Call<List<SchoolResponse>> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "onFailure: Connection Failure");
            }
        });
    }

}